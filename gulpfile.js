const gulp = require('gulp')
const browserSync = require('browser-sync').create()
const concat = require('gulp-concat')
const changed = require('gulp-changed')
const flatten = require('gulp-flatten')
const gulpif = require('gulp-if')
const image = require('gulp-image')
const inject = require('gulp-inject')
const plumber = require('gulp-plumber')
const notify = require('gulp-notify')
const autoprefixer = require('gulp-autoprefixer')
const scss = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')
const del = require('del')
const runSequence = require('gulp4-run-sequence')
const terser = require('gulp-terser')
const yargs = require('yargs')

const PROD = yargs.argv.production;

const src = './src'
const dest = './public/assets'

const paths = {
	public: dest,
	bootstrap: {
		src: `${src}/scss/bootstrap/bootstrap.scss`,
		watch: `${src}/scss/bootstrap/**/*.scss`,
		dest: `${dest}/css/`,
	},
	styles: {
		src: `${src}/scss/main.scss`,
		watch: `${src}/scss/**/*.+(scss|css)`,
		dest: `${dest}/css/`,
	},
	html: {
		src: `${src}/**/*.html`,
		watch: `${src}/**/*.html`,
		dest: `./public/`,
	},
	fonts: {
		src: `${src}/fonts/**/*.+(eot|svg|ttf|woff|woff2)`,
		dest: `${dest}/fonts/`
	},
	js: {
		src: [
			`${src}/js/vendor/jquery-3.6.0.js`,
			`${src}/js/vendor/bootstrap.bundle.js`,
			`${src}/js/vendor/jquery.mark.js`,
			`${src}/js/vendor/simplePagination.js`,
			`${src}/js/scripts.js`,
		],
		dest: `${dest}/js/`
	},
	img: {
		src: `${src}/img/**/*.+(jpg|jpeg|png|svg|gif|ico)`,
		dest: `${dest}/img/`
	},
};

gulp.task('clean:public', () => {
	return del('./public');
});

gulp.task('server', () => {
	browserSync.init({
		port: 5000,
		server: {
			baseDir: './public'
		},
		reloadOnRestart: true,
		open: false,
		ghostMode: true,
		notify: {
			styles: {
				top: '0',
				padding: '4px 8px',
				fontSize: '13px',
				background: '#fff',
				color: '#666',
				borderBottomLeftRadius: '0'
			}
		}
	});

	gulp.watch(paths.html.watch, gulp.series('copy:html'));
	gulp.watch(paths.styles.watch, gulp.series('build:css'));
	gulp.watch(paths.js.src, gulp.series('build:js'));
	gulp.watch(paths.fonts.src, gulp.series('copy:fonts'));
	gulp.watch(paths.img.src, gulp.series('copy:img'));
});

gulp.task('copy:fonts', () => {
	return gulp.src(paths.fonts.src)
		.pipe(flatten())
		.pipe(gulp.dest(paths.fonts.dest))
		.pipe(browserSync.stream());
});

gulp.task('copy:html', () => {
	return gulp.src(paths.html.src)
		.pipe(changed(paths.html.dest))
		.pipe(gulp.dest(paths.html.dest))
		.pipe(browserSync.stream());
});

gulp.task('copy:img', () => {
	return gulp.src(paths.img.src)
		.pipe(changed(paths.img.dest))
		.pipe(image({
      pngquant: true,
      optipng: false,
      zopflipng: true,
      jpegRecompress: true,
      mozjpeg: true,
      gifsicle: true,
      svgo: true,
      concurrent: 10,
      quiet: true // defaults to false
    }))
		.pipe(gulp.dest(paths.img.dest))
		.pipe(browserSync.stream());
});

gulp.task('build:js', () => {
	return gulp.src(paths.js.src)
		.pipe(plumber({
			errorHandler: notify.onError((e) => {
				return {
					title: 'Javascript',
					message: e.message
				}
			})
		}))
		.pipe(gulpif(!PROD, sourcemaps.init()))
		.pipe(concat('bundle.js'))
		.pipe(gulpif(PROD, terser({
			// output: { comments: false }
		})))
		.pipe(gulpif(!PROD, sourcemaps.write('maps')))
		.pipe(gulp.dest(paths.js.dest))
		.pipe(browserSync.stream());
});

gulp.task('build:css', () => {
	return gulp.src(paths.styles.src)
		.pipe(plumber({
			errorHandler: notify.onError((e) => {
				return {
					title: 'Styles CSS',
					message: e.message
				}
			})
		}))
		.pipe(gulpif(!PROD, sourcemaps.init()))
		.pipe(scss({ outputStyle: !PROD ? 'expanded' : 'compressed' })) // expanded, compact, compressed, nested
		.pipe(autoprefixer({
			overrideBrowserslist: ['last 5 versions'],
			cascade: false
		}))
		// .pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulpif(!PROD, sourcemaps.write('maps')))
		.pipe(gulp.dest(paths.styles.dest))
		.pipe(browserSync.stream());
});


gulp.task('default', (done) => {
	runSequence(
		'clean:public',
		['build:js', 'build:css', 'copy:fonts', 'copy:html', 'copy:img'],
		'server',
		done
	)
});

gulp.task('build', (done) => {
	runSequence(
		'clean:public',
		['build:js', 'build:css', 'copy:fonts', 'copy:html', 'copy:img'],
		done
	)
});

gulp.task('clean', (done) => {
	runSequence(
		'clean:public',
		done
	)
});
