$(function () {
  // Keyword highlight
  var mark = function () {
    var keyword = $('input[name="keyword"]').val();
    $('.context').unmark({
      done: function () {
        $('.context').mark(keyword);
      }
    });
  };
  $('input[name="keyword"]').on('input', mark);

  // Random Slider
  const slideItem = $('#mainSlider .carousel-item').length;
  const randomNum = Math.floor(Math.random() * slideItem) + 1;
  const randomNumIndex = randomNum - 1;
  $('.carousel').carousel(randomNumIndex);
  $('.carousel-item').removeClass('transparent');


  $('.btn-social').click(function (e) {
    e.preventDefault();
    alert('Wow, You clicked a link!');
  });

  // Add extra hidden text to posts
  $('.post p').after('<p class="moretext">Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, delectus provident asperiores obcaecati exercitationem minus atque assumenda nobis maxime. Voluptate nostrum neque sint illo alias.</p>');

  // Show/hide extra text
  var triggerButton = $('.readmore').html();
  $('.moretext').hide();
  $('.readmore').addClass('closed');

  $('.readmore').click(function (e) {
    e.preventDefault();
    $(this).prev().slideToggle(250, function () {
      if ($(this).is(':hidden')) {
        $(this).next().removeClass('open btn-secondary');
        $(this).next().addClass('closed btn-primary');
        $(this).next().html(triggerButton);
      } else {
        $(this).next().removeClass('closed btn-primary');
        $(this).next().addClass('open btn-secondary');
        $(this).next().html('Read less');
      }
    });
  });



  // Post Paginator
  var items = $('.blog .post');
  var itemNumber = items.length;
  var pageCount = 3;

  items.slice(pageCount).hide();

  $('#pagination').pagination({
    items: itemNumber,
    itemsOnPage: pageCount,
    prevText: '&laquo;',
    nextText: '&raquo;',
    onPageClick: function (pageNumber) {
      var showFrom = pageCount * (pageNumber - 1);
      var showTo = showFrom + pageCount;
      items.hide().slice(showFrom, showTo).show();
    }
  });

});
